package com.ihor_chikh.passive.controller;

import com.ihor_chikh.passive.model.Model;

import com.ihor_chikh.passive.view.View;

import java.util.Scanner;


public class Controller {
    private Model model;
    private View view;

    {
        this.model = new Model();
        this.view = new View();
    }

    public void run() {
        view.mainMenu();
        Scanner scanner = new Scanner(System.in);
        while (true) {
            if (scanner.hasNextInt()) {
                int result = scanner.nextInt();
                try {
                    view.displayStudent(model.getStudentInfo(result - 1));
                } catch (IndexOutOfBoundsException ex) {
                    view.displayWarningMessage(model.getStudentsList().size());
                }

            } else if (scanner.hasNextLine()) {
                String result = scanner.nextLine();
                if (result.equals("close")) {
                    break;
                }
                System.out.println(result);
            }
        }
    }
}
