package com.ihor_chikh.passive.view;


import java.util.Scanner;

public class View {
    Scanner scanner = new Scanner(System.in);

    public void mainMenu() {
        System.out.println("Input:" +
                "\n" +
                "index to get student info;" +
                "\n" +
                "'close' - to quit.");
    }


    private String addStudent() {
        System.out.println("add student name: ");
        return scanner.nextLine();
    }

    public void displayStudent(String studentName) {
        System.out.println(studentName);
    }

    public void displayWarningMessage(int qty) {
        System.out.println("There are only " + qty + " student(s)");
    }
}
