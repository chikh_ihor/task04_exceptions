package com.ihor_chikh.passive.model;

public class Student {
    private String name;
    private String description;
    private int mark;

    Student(String name) {
        //values by default
        this.name = name;
        this.description = "good boy.";
        this.mark = 5;
    }

    Student(String name, String description, int mark) {
        this.name = name;
        this.description = description;
        this.mark = mark;
    }

    public String getName() {
        return name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public int getMark() {
        return mark;
    }

    @Override
    public String toString() {
        return ("Student " +
                "\n" +
                "name: " + name +
                "\n" +
                "description: " + description +
                "\n" +
                "mark: " + mark);
    }
}
