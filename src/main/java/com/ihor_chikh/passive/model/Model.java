package com.ihor_chikh.passive.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Model {
    private List<Student> studentsList = new ArrayList<>();

    {
        studentsList.add(new Student("Bogdan", "bad boy.", 2));
        studentsList.add(new Student("Vasyl"));
        studentsList.add(new Student("Petro", "bad boy.", 2));
        studentsList.add(new Student("Oleh"));
        studentsList.add(new Student("Vitaliy", "not bad boy.", 3));
        studentsList.add(new Student("Taras"));
        sortStudentListByName();
    }

    public void addStudent(String studentName) {
        studentsList.add(new Student(studentName));
        sortStudentListByName();
    }

    public String getStudentName(int listIndex){
        sortStudentListByName();
        return studentsList.get(listIndex).getName();
    }

    public String getStudentInfo(int listIndex){
        sortStudentListByName();
        return studentsList.get(listIndex).toString();
    }

    private void sortStudentListByName() {
        studentsList.sort(Comparator.comparing(Student::getName));
    }

    public List<Student> getStudentsList() {
        return studentsList;
    }
}
