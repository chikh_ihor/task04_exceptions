package com.ihor_chikh.passive;

import com.ihor_chikh.passive.controller.Controller;
import com.ihor_chikh.passive.model.Model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

class Main {
    public static void main(String[] args) {
       /* new Controller().run();
        try {
            createFile("file.txt");
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        deleteFile("file.txt");*/
        try {
            method();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        try {
            int a = parseInt("2w1");
            System.out.println(a);
        } catch (RuntimeException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static void method() throws Exception {
        String message = "eee";
        throw new Exception(message);
    }

    public static int parseInt(String s) throws RuntimeException {
        return Integer.parseInt(s);
    }


    public static void createFile(String name) throws IOException {
        //absolute file name with path

        String absoluteFilePath = "C:\\Users\\Admin\\Desktop\\epam_java_tasks\\task04_exceptions\\src\\main\\resources\\" + name;
        File file = new File(absoluteFilePath);
        if (file.createNewFile()) {
            System.out.println(name + " File Created");
        } else System.out.println("File " + absoluteFilePath + " already exists");
    }

    public static void deleteFile(String name) {
        String absoluteFilePath = "C:\\Users\\Admin\\Desktop\\epam_java_tasks\\task04_exceptions\\src\\main\\resources\\" + name;
        File file = new File(absoluteFilePath);
        if (file.delete()) {
            System.out.println(name + " File Deleted");
        } else System.out.println(name + "File " + absoluteFilePath + " already deleted");

    }
}